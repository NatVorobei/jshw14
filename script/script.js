let themeBtn = document.getElementById('change_btn');
let backCl = document.querySelector('.wrapper');

let getTheme = localStorage.getItem('theme');
if(getTheme && getTheme === 'dark_theme'){
    backCl.classList.add('dark_theme');
}

themeBtn.addEventListener('click', () => {
    backCl.classList.toggle('dark_theme');

    if(!backCl.classList.contains('dark_theme')){
        return localStorage.setItem('theme', 'light_theme');
    }
    localStorage.setItem('theme', 'dark_theme');

})

